package com.b201crew.protel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.b201crew.protel.helper.ScreenHelper

class ContentHeaderFragment : Fragment(), View.OnClickListener {

    private lateinit var backButton : Button
    private lateinit var screenHelper: ScreenHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        val contentView = inflater.inflate(R.layout.fragment_header_content, container, false)

        backButton = contentView.findViewById(R.id.back_button)
        backButton.setOnClickListener(this)

        screenHelper = ScreenHelper(requireActivity())

        return contentView
    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            R.id.back_button -> screenHelper.closeScreen()
        }
    }
}