package com.b201crew.protel

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentTimerMainBinding
import com.b201crew.protel.helper.ScreenHelper
import com.b201crew.protel.helper.SharedHelper
import com.b201crew.protel.room.AppDatabase
import java.text.SimpleDateFormat
import java.util.*

class SmallTimeFragment : Fragment(), View.OnClickListener {

    private lateinit var screenHelper: ScreenHelper
    private lateinit var binding: FragmentTimerMainBinding
    private lateinit var appDb: AppDatabase
    private lateinit var sharedHelper: SharedHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_timer_main, container, false)

        binding.leftContainer.setOnClickListener(this)
        binding.rightContainer.setOnClickListener(this)

        //menunjukkan isian sementara
        binding.totalTimeContent.text = getString(R.string.time_left_content, "0", "0")
        binding.nextTreatmentContent.text = getString(R.string.time_left_content, "0", "0")

        screenHelper = ScreenHelper(requireActivity())
        appDb = AppDatabase.getInstance(requireContext())
        sharedHelper = SharedHelper(requireContext())
        requireActivity().registerReceiver(br, IntentFilter("com.b201crew.protel.treatment_timer"))

        getTimer()
        getNextTreatment()
        return binding.root

    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.left_container -> screenHelper.switchScreen(requireContext(), TimeHistoryActivity::class.java)
            R.id.right_container -> screenHelper.switchScreen(requireContext(), TreatmentActivity::class.java)
        }
    }

    private fun getTimer() {
        val tempDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
        val startDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(tempDate)
        val endDate = Calendar.getInstance()
        endDate.time = startDate!!
        endDate.add(Calendar.DATE, 1)

        val currentTotalTime = appDb.TimerDao().getTotalFilterByDate(endDate = endDate.timeInMillis, startDate = startDate.time)
        binding.totalTimeContent.text = getString(R.string.time_left_content, (currentTotalTime / 60).toString(), (currentTotalTime % 60).toString())
    }

    private fun getNextTreatment() {
        val nextTreatment = sharedHelper.getTreatmentLeft() / 1000
        binding.nextTreatmentContent.text = getString(R.string.time_left_content, (nextTreatment / 60).toString(), (nextTreatment % 60).toString())
    }

    private val br = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val timeLeft: Long = intent?.getLongExtra("timer", 0)!! //get timer  left
            val minutes = timeLeft / 60
            val seconds = timeLeft % 60
            Log.d("HEHE", timeLeft.toString())

            binding.nextTreatmentContent.text = getString(R.string.time_left_content, minutes.toString(), seconds.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        requireActivity().unregisterReceiver(br)
    }
}
