package com.b201crew.protel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentHistoryAllBinding
import com.b201crew.protel.room.AppDatabase
import java.text.SimpleDateFormat
import java.util.*

class TimeHistoryAllFragment : Fragment() {

    private lateinit var binding: FragmentHistoryAllBinding
    private lateinit var appDb: AppDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history_all, container, false)

        appDb = AppDatabase.getInstance(requireContext())

        getTempData()
        return binding.root
    }

    private fun getTempData() {
        val tempDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date()) //get current date and convert it to "yyyy-MM-dd"
        val startDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(tempDate) //parse from "yyyy-MM-dd" to Date type
        val endDate = Calendar.getInstance()
        endDate.time = startDate!!
        endDate.add(Calendar.DATE, 1)

        setContentText(startDate.time, endDate.timeInMillis)
    }

    fun setContentText(startDate: Long, endDate: Long) {
        val totalTime = appDb.TimerDao().getTotalFilterByDate(startDate, endDate)
        val totalPosture = appDb.PostureDao().getCountFilterByDate(startDate, endDate)

        binding.postureWarningContent.text = totalPosture.toString()
        binding.totalAllTimeContent.text = getString(R.string.time_left_content, (totalTime / 60).toString(), (totalTime % 60).toString())
    }

}