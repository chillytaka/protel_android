package com.b201crew.protel.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Digunakan untuk menyimpan waktu saat alat mendeteksi postur tubuh yang salah
 */
@Entity(tableName = "posture")
data class PostureModel (
        @PrimaryKey(autoGenerate = true) val id: Int,
        @ColumnInfo(name = "timestamp") val timestamp: Long
)