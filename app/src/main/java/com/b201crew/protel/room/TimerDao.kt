package com.b201crew.protel.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface TimerDao {
    @Query("SELECT * FROM timer")
    fun getAll(): List<TimerModel>

    @Query("SELECT SUM(length_time) FROM timer WHERE timestamp >= :startDate AND timestamp <= :endDate ")
    fun getTotalFilterByDate(startDate: Long, endDate: Long): Long

    @Query("SELECT MAX(length_time) FROM timer WHERE timestamp >= :startDate AND timestamp <= :endDate ")
    fun getMaxTimerFilterByDate(startDate: Long, endDate: Long): Long

    @Query("SELECT MIN(length_time) FROM timer WHERE timestamp >= :startDate AND timestamp <= :endDate ")
    fun getMinTimerFilterByDate(startDate: Long, endDate: Long): Long

    @Insert
    fun insert(vararg timer: TimerModel)

    @Query("DELETE FROM timer")
    fun deleteAll()
}