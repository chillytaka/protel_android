package com.b201crew.protel.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = arrayOf(PostureModel::class, TimerModel::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        private var instance: AppDatabase? = null
        private const val DB_NAME = "protel_db"

        fun getInstance(context: Context): AppDatabase {
            //FIXME: remove allowMainThreadQueries
            instance = instance?: Room.databaseBuilder(context, AppDatabase::class.java, DB_NAME).allowMainThreadQueries().build()
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }

    }

    abstract fun TimerDao():  TimerDao
    abstract fun PostureDao(): PostureDao
}
