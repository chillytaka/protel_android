package com.b201crew.protel.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Digunakan untuk menyimpan berapa lama waktu duduk
 */
@Entity(tableName= "timer")
data class TimerModel (
    @PrimaryKey (autoGenerate = true) val id: Int?,
    @ColumnInfo(name = "length_time") val length_time: Int, //lama waktu duduk dalam detik
    @ColumnInfo(name = "timestamp") val timestamp: Long //waktu value disimpa
)