package com.b201crew.protel.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PostureDao {
    @Query("SELECT *  FROM posture")
    fun getAll(): List<PostureModel>

    @Query("SELECT COUNT(*) FROM posture WHERE timestamp >= :startDate AND timestamp <= :endDate ")
    fun getCountFilterByDate(startDate: Long, endDate: Long): Int

    @Insert
    fun insert(vararg posture: PostureModel)

    @Query("DELETE FROM posture")
    fun deleteAll()
}