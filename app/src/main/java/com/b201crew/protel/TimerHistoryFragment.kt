package com.b201crew.protel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentHistoryTimerBinding
import com.b201crew.protel.helper.DatePickerHelper
import java.util.*

class TimerHistoryFragment : Fragment(), View.OnClickListener, DatePickerHelper.DatePickerInterface {

    private lateinit var buttonPushed: String //store which button being pushed
    private lateinit var binding: FragmentHistoryTimerBinding
    private lateinit var startDate: Date
    private lateinit var endDate: Date

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history_timer, container, false)

        binding.startDateButton.setOnClickListener(this)
        binding.endDateButton.setOnClickListener(this)

        return binding.root
    }

    private fun getDate(buttonPushed: String) {
        val picker = DatePickerHelper()
        picker.setDatePickerInterface(this)
        picker.show(requireFragmentManager(), "Date Picker")

        this.buttonPushed = buttonPushed //set which button is being pushed
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.start_date_button -> getDate("start")
            R.id.end_date_button -> getDate("end")
        }
    }

    //updating button text after date got selected
    override fun onDateSelected(date: String, calendar: Calendar) {
        if (buttonPushed == "start") {
            binding.startDateButton.text = date
            startDate = calendar.time
        } else {
            binding.endDateButton.text = date
            endDate = calendar.time
            applyChange()
        }
    }

    /**
     * apply date change to other fragment that needed it
     */
    private fun applyChange() {
        val fm = requireFragmentManager()
        val minMaxFragment: TimeHistoryFragment = fm.findFragmentById(R.id.sit_time_history) as TimeHistoryFragment
        val timerAllFragment: TimeHistoryAllFragment = fm.findFragmentById(R.id.sit_all_history) as TimeHistoryAllFragment
        minMaxFragment.setMaxMinText(startDate = startDate.time, endDate = endDate.time)
        timerAllFragment.setContentText(startDate = startDate.time, endDate = endDate.time)
    }
}