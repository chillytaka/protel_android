package com.b201crew.protel

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentTreatmentManualBinding
import com.b201crew.protel.service.BluetoothService

class TreatmentManualFragment : Fragment(), View.OnClickListener {

    private var treatmentState: Boolean = false
    private lateinit var binding: FragmentTreatmentManualBinding
    private var status = false
    private var mBounded = false
    private var service: BluetoothService? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_treatment_manual, container, false)

        binding.manualPowerButton.setOnClickListener(this)
        connectService()

        return binding.root
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.manual_power_button -> toggleTreatment()
        }
    }

    private fun toggleTreatment() {
        if (treatmentState) {

            service!!.sendMsg("0")
            treatmentState = false
            binding.manualPowerButton.setColorFilter(resources.getColor(R.color.lightBlack, null))
            binding.treatmentOffText.setTextColor(resources.getColor(R.color.colorAccent, null))
            binding.treatmentOnText.setTextColor(resources.getColor(R.color.lightBlack, null))

        } else {

            service!!.sendMsg("1")
            treatmentState = true
            binding.manualPowerButton.setColorFilter(resources.getColor(R.color.colorDanger, null))
            binding.treatmentOnText.setTextColor(resources.getColor(R.color.colorAccent, null))
            binding.treatmentOffText.setTextColor(resources.getColor(R.color.lightBlack, null))

        }
    }

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            mBounded = true
            val mLocalBinder = p1 as BluetoothService.LocalBinder
            service = mLocalBinder.getServerInstance()!!
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            mBounded = false
            service = null
            status = false
        }
    }

    private fun connectService() {
        val serviceIntent = Intent(context, BluetoothService::class.java)
        requireActivity().bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (mBounded) {
            mConnection.let { requireActivity().unbindService(mConnection) }
            mBounded = false
        }
    }

}

