package com.b201crew.protel.service

import android.app.*
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.*
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.b201crew.protel.MainActivity
import com.b201crew.protel.helper.BluetoothHelper
import com.b201crew.protel.helper.SharedHelper
import com.b201crew.protel.room.AppDatabase
import com.b201crew.protel.room.TimerModel
import java.io.IOException
import java.util.*

private const val CONNECTED_SUCCESSFULLY = 1
private const val CHAIR_OCCUPIED = 2
private const val CHAIR_UNOCCUPIED = 3
private const val SOCKET_ERROR = 666
private const val CHANNEL_ID: String = "BluetoothServiceChannel"
private const val NORMAL_ID: String = "ServiceChannel"
private const val NOTIFICATION_ID: Int = 20

class BluetoothService : Service() {
    private lateinit var notificationManager: NotificationManager
    private lateinit var thread: Thread
    private lateinit var bluetoothHelper: BluetoothHelper
    private lateinit var sharedHelper: SharedHelper
    private lateinit var appDb: AppDatabase
    private val mBinder: LocalBinder = LocalBinder()
    private var timeLeft: Long? = 0.toLong()
    private var isTimerRunning = false
    private var isTreatmentTimerRunning = false
    private var isSitAgainTimerRunning = false
    private var isHeaterOn = false

    override fun onCreate() {
        super.onCreate()

        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        createNotificationChannel()

        sharedHelper = SharedHelper(this)
        appDb = AppDatabase.getInstance(this)
        bluetoothConnect()
        sharedHelper.setServiceStatus(true)

        /**
         * init notification, this is needed for the requirements of Foreground Services
         */
        val notification = showNotification("chair is not connected")
        Toast.makeText(this, "Service Started", Toast.LENGTH_SHORT).show()
        startForeground(NOTIFICATION_ID, notification)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        return START_STICKY
    }

    /**
     * Handler of the bluetooth thread, used to communicate between bluetooth thread and service
     */
    private val handler = object : Handler(Looper.getMainLooper()) {
        override fun handleMessage(msg: Message) {
            super.handleMessage(msg)

            when(msg.what) {
                CONNECTED_SUCCESSFULLY -> showNotification("chair is connected")
                CHAIR_OCCUPIED -> startCountDown()
                CHAIR_UNOCCUPIED -> stopCountdown()
                SOCKET_ERROR -> restartBluetooh()
            }

        }

    }

    /**
     * helper to replace notification
     */
    private fun showNotification(msg: String): Notification {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val notification : Notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("ProTel")
                .setContentText(msg)
                .setSmallIcon(android.R.drawable.ic_media_play)
                .setContentIntent(pendingIntent)
                .build()

        notificationManager.notify(NOTIFICATION_ID, notification)

        return notification
    }

    /**
     * helper to replace notification
     */
    private fun showNormalNotification(msg: String): Notification {
        val notificationIntent = Intent(this, MainActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0)

        val notification: Notification = NotificationCompat.Builder(this, NORMAL_ID)
                .setContentTitle("ProTel")
                .setContentText(msg)
                .setSmallIcon(android.R.drawable.ic_dialog_alert)
                .setContentIntent(pendingIntent)
                .build()

        notificationManager.notify(80, notification)

        return notification
    }

    private fun restartBluetooh() {
        showNotification("chair is disconnected")
        bluetoothHelper.disconnect()
        bluetoothConnect()
    }

    /**
     * Start Bluetooth Helper thread to connect to bluetooth
     */
    private fun bluetoothConnect() {
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

        val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
        pairedDevices?.forEach {device ->
            if (device.address == "00:19:06:34:F0:85") {
                Log.d("BluetoothConnect", "FOUND")
                bluetoothHelper = BluetoothHelper(device, handler)
                thread = Thread(bluetoothHelper)
                thread.start()
            }
        }
    }

    //send message to bluetooth helper
    fun sendMsg(msg:String) {
        try{
            bluetoothHelper.sendMsg(msg)
        } catch (e: IOException) {}
        Log.e("Bluetooth Service", "Unable to send msg")
    }

    private fun startTreatmentCountdown() {
        if (!isTreatmentTimerRunning) {
            val intent = Intent(this, TimerServiceTreatment::class.java)
            intent.putExtra("broadcastId", "com.b201crew.protel.treatment_timer")

            if (!isHeaterOn) {
                if (sharedHelper.getTreatmentLeft() == 0.toLong()) {
                    intent.putExtra("timeLeft", 1800000.toLong())
                } else {
                    intent.putExtra("timeLeft", sharedHelper.getTreatmentLeft())
                }
            } else {
                intent.putExtra("timeLeft", 600000.toLong())
            }

            startService(intent)
            registerReceiver(bc, IntentFilter("com.b201crew.protel.treatment_timer"))
            isTreatmentTimerRunning = true
        }
    }

    private fun startCountDown() {
        startTreatmentCountdown()
        if (!isTimerRunning) {
            if (isSitAgainTimerRunning) {
                stopSitAgainCountdown()
            }
            val intent = Intent(this, TimerService::class.java)

            //mengeset broadcast receiver id
            intent.putExtra("broadcastId", "com.b201crew.protel.countdown_test")

            /**
             * mengeset berapa banyak waktu untuk di countdown
             * kalau sisa waktu sebelumnya masih ada, countdown sisa waktu tersebut
             */
            if (sharedHelper.getSitTime() == 0.toLong()) {
                intent.putExtra("timeLeft", 900000.toLong())
            } else {
                intent.putExtra("timeLeft", sharedHelper.getSitTime())
            }

            startService(intent)

            registerReceiver(br, IntentFilter("com.b201crew.protel.countdown_test"))

            isTimerRunning = true
        }
    }

    private fun startSitAgainCountdown() {
        if (!isSitAgainTimerRunning) {
            val intent = Intent(this, TimerService::class.java)

            //mengeset broadcast receiver id
            intent.putExtra("broadcastId", "com.b201crew.protel.countdown_left")
            intent.putExtra("timeLeft", 10000.toLong())

            startService(intent)
            registerReceiver(rc, IntentFilter("com.b201crew.protel.countdown_left"))
            isSitAgainTimerRunning = true
        }
    }

    private fun stopCountdown() {
        if (isTreatmentTimerRunning && !isHeaterOn) {
            stopTreatmentCountdown()
        }
        if (isTimerRunning) {
            try {
                unregisterReceiver(br) //unregister broadcast receiver
            } catch (e: Exception) {
                Log.d("onDestroy Exception", e.toString())
            }
            stopService(Intent(this, TimerService::class.java)) //stopping timerService and cancel timer
            startSitAgainCountdown()
            isTimerRunning = false
        }
    }

    private fun stopTreatmentCountdown() {
        if (isTreatmentTimerRunning) {
            try {
                unregisterReceiver(bc) //unregister broadcast receiver
            } catch (e: Exception) {
                Log.d("onDestroy Exception", e.toString())
            }
            stopService(Intent(this, TimerServiceTreatment::class.java)) //stopping timerService and cancel timer
            isTreatmentTimerRunning = false
        }
    }

    private fun stopSitAgainCountdown() {
        if (isSitAgainTimerRunning) {
            try {
                unregisterReceiver(rc) //unregister broadcast receiver
            } catch (e: Exception) {
                Log.d("onDestroy Exception", e.toString())
            }
            stopService(Intent(this, TimerService::class.java)) //stopping timerService and cancel timer
            isSitAgainTimerRunning = false
        }
    }

    private var rc = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val timerLeft: Long? = intent?.getLongExtra("timer", 0)
            if (timerLeft == 0.toLong()) {
                insertCurentTime()
            }
        }
    }

    private var bc = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val timerLeft: Long? = intent?.getLongExtra("timer", 0)
            Log.d("COBA", timerLeft.toString())
            sharedHelper.writeTreatmentLeft(timerLeft!! * 1000)
            if (timerLeft == 0.toLong()) {
                toggleHeater()
                startTreatmentCountdown()
            }
        }
    }

    private var br =object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            timeLeft = intent?.getLongExtra("timer", 0)
            Log.d("BAM", timeLeft.toString())
            sharedHelper.writeSitTime(timeLeft!! * 1000)
            if (timeLeft == 0.toLong()) {
                showNormalNotification("silahkan berdiri dari kursi dan bergerak !")
            }
        }
    }

    private fun toggleHeater() {
        isHeaterOn = if (isHeaterOn) {
            sendMsg("0")
            false
        } else {
            sendMsg("1")
            true
        }
    }

    private fun insertCurentTime() {
        appDb.TimerDao().insert(TimerModel(id = null, length_time = 900 - timeLeft!!.toInt(), timestamp = Calendar.getInstance().timeInMillis))
        sharedHelper.deleteSitTime()
    }

    //set up notification channel ( only used for android O and above )
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                    CHANNEL_ID, "Background Service Channel", NotificationManager.IMPORTANCE_LOW)

            notificationManager.createNotificationChannel(serviceChannel)
            notificationManager.createNotificationChannel(NotificationChannel(NORMAL_ID, "Alerts Notification", NotificationManager.IMPORTANCE_DEFAULT))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        bluetoothHelper.disconnect() //disconnecting bluetooth
        try {
            unregisterReceiver(br) //unregister broadcast receiver
            unregisterReceiver(rc) //unregister broadcast receiver
        }  catch (e: Exception) {Log.d("onDestroy Exception", e.toString())}

        stopService(Intent(this, TimerService::class.java)) //stopping timerService and cancel timer
        Toast.makeText(this, "Service Stopped", Toast.LENGTH_SHORT).show()
        sharedHelper.setServiceStatus(false)
    }

    override fun onBind(p0: Intent?): IBinder? {
        return mBinder
    }

    inner class LocalBinder: Binder() {
        fun getServerInstance(): BluetoothService? {
            return this@BluetoothService //return the current instance
        }
    }
}