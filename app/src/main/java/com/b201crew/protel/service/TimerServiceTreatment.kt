package com.b201crew.protel.service

import android.app.Service
import android.content.Intent
import android.os.CountDownTimer
import android.os.IBinder
import android.util.Log

/**
 * FIXME: KODE INI HARUS DIGANTI KARENA ISINYA SAMA PERSIS DENGAN TIMERSERVICE
 */
class TimerServiceTreatment : Service() {
    private lateinit var intent: Intent
    private lateinit var cdt: CountDownTimer

    override fun onCreate() {
        super.onCreate()
        Log.d("MUST", "onCreate: HORE")
    }

    override fun onStartCommand(service: Intent?, flags: Int, startId: Int): Int {
        val timeLeft = service!!.getLongExtra("timeLeft", 0)

        val broadcastId = service.getStringExtra("broadcastId")
        val test = Math.random()
        Log.d("Test", "$test : $broadcastId!!")

        intent = Intent()
        intent.action = broadcastId!!

        cdt = object : CountDownTimer(timeLeft, 1000) {
            override fun onFinish() {
                Log.d("Timer Broadcast", "timer finished")
                stopSelf()
            }

            //sent broadcast every second so we can observe every seconds
            override fun onTick(msLeft: Long) {
                intent.putExtra("timer", msLeft / 1000)
                sendBroadcast(intent)
            }
        }

        cdt.start()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        cdt.cancel()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}