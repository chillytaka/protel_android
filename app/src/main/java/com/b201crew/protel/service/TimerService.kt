package com.b201crew.protel.service

import android.app.Service
import android.content.Intent
import android.os.CountDownTimer
import android.os.IBinder
import android.util.Log

/**
 * Timer Service, used for counting how much time left every user session
 */
class TimerService: Service() {
    private lateinit var intent: Intent
    private lateinit var cdt: CountDownTimer

    override fun onCreate() {
        super.onCreate()
        Log.d("DUARR", "1")
    }

    override fun onStartCommand(service: Intent?, flags: Int, startId: Int): Int {
        val timeLeft = service!!.getLongExtra("timeLeft", 0)

        val broadcastId = service.getStringExtra("broadcastId")
        val test = Math.random()
        Log.d("FUU", "$test : $broadcastId!!")
        intent = Intent()
        intent.action = broadcastId!!

        cdt = object : CountDownTimer(timeLeft, 1000) {
            override fun onFinish() {
                Log.d("Timer Broadcast", "timer finished")
                stopSelf()
            }

            //sent broadcast every second so we can observe every seconds
            override fun onTick(msLeft: Long) {
                intent.putExtra("timer", msLeft / 1000)
                Log.d("COBA2", "$broadcastId : $msLeft")
                sendBroadcast(intent)
            }
        }

        cdt.start()
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        cdt.cancel()
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}