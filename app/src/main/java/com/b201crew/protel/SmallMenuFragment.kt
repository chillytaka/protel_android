package com.b201crew.protel

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentMenuMainBinding
import com.b201crew.protel.helper.ScreenHelper
import com.b201crew.protel.room.AppDatabase
import com.b201crew.protel.service.BluetoothService

class SmallMenuFragment : Fragment(), View.OnClickListener {

    private lateinit var screenHelper: ScreenHelper
    private var status = false
    private var mBounded = false
    private var service: BluetoothService? = null
    private lateinit var appDb: AppDatabase
    private lateinit var binding: FragmentMenuMainBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu_main, container, false)

        binding.settingsButton.setOnClickListener(this)
        binding.treatmentSettingsButton.setOnClickListener(this)

        //screen handler to help on changing screen
        screenHelper = ScreenHelper(requireActivity())

        return binding.root
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.settings_button -> screenHelper.switchScreen(requireContext(), SettingsActivity::class.java)
            //R.id.settings_button -> toggleService()
            R.id.treatment_settings_button -> screenHelper.switchScreen(requireContext(), TreatmentSettingsActivity::class.java)
            //R.id.treatment_settings_button -> runServiceMethod()
        }
    }

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            Toast.makeText(context, "Service Connected Successfully", Toast.LENGTH_SHORT).show()
            mBounded = true
            val mLocalBinder = p1 as BluetoothService.LocalBinder
            service = mLocalBinder.getServerInstance()!!
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            Toast.makeText(context, "Service Disconnected Successfully", Toast.LENGTH_SHORT).show()
            mBounded = false
            service = null
            status = false
        }
    }

    private fun connectService() {
        val serviceIntent = Intent(context, BluetoothService::class.java)
        requireActivity().bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    private fun toggleService() {
        val serviceIntent = Intent(context, BluetoothService::class.java)
        if (!status) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context?.startForegroundService(serviceIntent)
            } else {
                context?.startService(serviceIntent)
            }
            connectService()
            status = true
        } else {
            if (mBounded) {
                mConnection.let { requireActivity().unbindService(mConnection) }
                mBounded = false
            }
            requireContext().stopService(serviceIntent)
            status = false
        }
    }

    /*
    private fun bluetoothConnect() {
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

        if (bluetoothAdapter == null) {
            Log.d("BluetoothConnect", "No Adapter found")
        }

        if (bluetoothAdapter?.isEnabled == false) {
            val enableBt = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBt, REQUEST_ENABLE_BT)
        }

        val pairedDevices: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
        pairedDevices?.forEach {device ->
            if (device.address == "00:19:06:34:F0:85") {
                Log.d("BluetoothConnect", "FOUND")
                bluetoothHelper = BluetoothHelper(device, handler)
                thread = Thread(bluetoothHelper)
                thread.start()
            }
        }
    }
     */
}
