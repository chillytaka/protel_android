package com.b201crew.protel

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentTreatmentNextBinding
import com.b201crew.protel.helper.SharedHelper
import java.text.SimpleDateFormat
import java.util.*

class TreatmentNextFragment : Fragment() {

    private lateinit var binding: FragmentTreatmentNextBinding
    private lateinit var sharedHelper: SharedHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_treatment_next, container, false)

        sharedHelper = SharedHelper(requireContext())
        getDummyData()

        return binding.root
    }

    private fun getDummyData() {
        val nextTreatment = sharedHelper.getTreatmentLeft()
        val tempDate = Calendar.getInstance()
        tempDate.add(Calendar.MILLISECOND, nextTreatment.toInt())

        val date = SimpleDateFormat("HH:mm", Locale.getDefault()).format(tempDate.time)

        Log.d("HEHE", date)
        Log.d("HEHE", nextTreatment.toString())

        binding.treatmentLengthContent.text = getString(R.string.time_left_content, "10", "0")
        binding.nextTreatmentTimeText.text = date

    }

}