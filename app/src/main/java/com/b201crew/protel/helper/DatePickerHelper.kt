package com.b201crew.protel.helper

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Bundle
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import java.text.SimpleDateFormat
import java.util.*

class DatePickerHelper : DialogFragment(), DatePickerDialog.OnDateSetListener{

    /**
     * Interfacing selected state of the DatePicker to be used for other class
     */
    interface DatePickerInterface{
        fun onDateSelected(date: String, calendar: Calendar)
    }

    private lateinit var dateInterface: DatePickerInterface

    fun setDatePickerInterface(dateInterface: DatePickerInterface) {
        this.dateInterface = dateInterface
    }

     override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
         val c = Calendar.getInstance()
         val year = c.get(Calendar.YEAR)
         val month = c.get(Calendar.MONTH)
         val day = c.get(Calendar.DAY_OF_MONTH)

        return DatePickerDialog(context!!, this, year, month, day)
    }

    //parsing selected date to string
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val c: Calendar = Calendar.getInstance()
        c.set(year,month,dayOfMonth)

        val dateTemp = SimpleDateFormat.getDateInstance()
        dateInterface.onDateSelected(dateTemp.format(c.time), c)
    }

}