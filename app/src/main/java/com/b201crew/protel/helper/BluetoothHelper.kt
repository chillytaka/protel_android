package com.b201crew.protel.helper

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.os.Handler
import android.os.SystemClock
import android.util.Log
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream

private const val CONNECTED_SUCCESSFULLY = 1
private const val CHAIR_OCCUPIED = 2
private const val CHAIR_UNOCCUPIED = 3
private const val SOCKET_ERROR = 666

class BluetoothHelper(private var bluetoothDevice: BluetoothDevice, private var handler: Handler) : Runnable {
    private lateinit var socket: BluetoothSocket
    private lateinit var msgInputStream: InputStream
    private lateinit var msgOutputStream: OutputStream
    private val msgBuffer: ByteArray = ByteArray(2048)
    private var running: Boolean = true
    private var prevValue = "0"

    override fun run() {
        var numBytes: Int

        Log.d("BluetoothHelper", "Successfully Started")
        try {
            socket = bluetoothDevice.createRfcommSocketToServiceRecord(bluetoothDevice.uuids[0].uuid)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        /**
         * This is to facilitate auto-connecting when the chair is nearby
         * the delay is in place to limit battery consumption of the device
         */
        Log.d("BluetoothHelper", "CONNECTING...")
        while (running) {
            try {
                socket.connect() //try to connect
                handler.sendEmptyMessage(CONNECTED_SUCCESSFULLY) //sent current status to service
                break
            } catch (e: IOException) {
                e.printStackTrace()

                //try again after waiting for 2 secs.
                SystemClock.sleep(2000)
            }
        }

        msgInputStream = socket.inputStream
        msgOutputStream = socket.outputStream

        /**
         * Read incoming connection
         */
        while (running) {
            numBytes = try {
                msgInputStream.read(msgBuffer)
            } catch (e: IOException) {
                Log.d("BluetoothRead", "Input Stream failed", e)
                handler.sendEmptyMessage(SOCKET_ERROR) //sent current status to service
                break
            }

            val text = String(msgBuffer, 0, numBytes) //convert to normal text

            if (prevValue != text && text.length == 1) {
                if (text == "2") {
                    Log.d("BluetoothRead", "STATUS:$CHAIR_OCCUPIED")
                    handler.sendEmptyMessage(CHAIR_OCCUPIED)
                } else if (text == "3") {
                    Log.d("BluetoothRead", "STATUS:$CHAIR_UNOCCUPIED")
                    handler.sendEmptyMessage(CHAIR_UNOCCUPIED)
                }
                prevValue = text
                Log.d("BluetoothRead", "Received Text:$text")
            }
        }

    }

    fun sendMsg(msg: String) {
        try {
            msgOutputStream.write(msg.toByteArray())
        } catch (e: IOException) {
            Log.e("BluetoothSend", "Cant send Data", e)
        }
    }

    fun disconnect() {
        try {
            socket.close()
            handler.removeCallbacks(this)
            running = false
            Log.d("BluetoothHelper", "Successfully Stopped")
        } catch (e: IOException) {
            Log.e("ClosingSocket", "Unable to close bluetooth socket", e)
        }
    }}