package com.b201crew.protel.helper

import android.content.Context

/**
 * Helper to read and write data from / to SharedPreferences
 */


@Suppress("PrivatePropertyName")
class SharedHelper(private val context: Context) {
    private val SIT_TIME = "com.b201crew.protel.shared_pref.time_left"
    private val SERVICE_STATUS = "com.b201crew.protel.shared_pref.service_status"
    private val TREATMENT_LEFT = "com.b201crew.protel.shared_pref.treatment_left"
    private val TREATMENT_LEFT_PER_DAY = "com.b201crew.protel.shared_pref.treatment_left_per_day"

    private val sharedPreference = context.getSharedPreferences("com.b201crew.protel.shared_pref", Context.MODE_PRIVATE)

    fun getSitTime(): Long {
        return sharedPreference.getLong(SIT_TIME, 900000)
    }

    fun writeSitTime(timeLeft: Long) {
        val editor = sharedPreference.edit()
        editor.putLong(SIT_TIME, timeLeft)
        editor.apply()
    }

    fun deleteSitTime() {
        val editor = sharedPreference.edit()
        editor.remove(SIT_TIME)
        editor.apply()
    }

    fun getTreatmentLeft(): Long {
        return sharedPreference.getLong(TREATMENT_LEFT, 1800000)
    }

    fun writeTreatmentLeft(timeLeft: Long) {
        val editor = sharedPreference.edit()
        editor.putLong(TREATMENT_LEFT, timeLeft)
        editor.apply()
    }

    fun getTreatmenPerDay(): Int {
        return sharedPreference.getInt(TREATMENT_LEFT_PER_DAY, 10)
    }

    fun setServiceStatus(status: Boolean) {
        val editor = sharedPreference.edit()
        editor.putBoolean(SERVICE_STATUS, status)
        editor.apply()
    }

    fun getServiceStatus(): Boolean {
        return sharedPreference.getBoolean(SERVICE_STATUS, false)
    }

}