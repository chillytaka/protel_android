package com.b201crew.protel.helper

import android.app.Activity
import android.content.Context
import android.content.Intent

class ScreenHelper(private var activity: Activity) {

    //facilitating screen switching across the app
    fun <T> switchScreen( context: Context, className:Class<T> ) {
        val intent = Intent(this.activity, className)
        context.startActivity(intent)
    }

    //finish an activity
    fun closeScreen() {
        this.activity.finish()
    }

}