package com.b201crew.protel

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentTimerLeftBinding
import com.b201crew.protel.helper.SharedHelper

class TimeLeftFragment : Fragment() {

    private lateinit var binding: FragmentTimerLeftBinding
    private lateinit var sharedHelper: SharedHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_timer_left, container, false)
        sharedHelper = SharedHelper(requireContext())

        //set default time as a placeholder
        val timeLeft = sharedHelper.getSitTime() / 1000
        binding.timeLeftContent.text = getString(R.string.time_left_content, (timeLeft / 60).toString(), (timeLeft % 60).toString())

        //receive timer broadcast
        requireActivity().registerReceiver(br, IntentFilter("com.b201crew.protel.countdown_test"))

        return binding.root
    }

    private val br = object: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val timeLeft:Long = intent?.getLongExtra("timer",0)!! //get timer  left
            val minutes = timeLeft/60
            val seconds = timeLeft % 60
            Log.d("TEST", timeLeft.toString())

            binding.timeLeftContent.text = getString(R.string.time_left_content, minutes.toString(), seconds.toString())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            requireActivity().unregisterReceiver(br)
        } catch (e: Exception) {
            Log.d("onDestroy Exception", e.toString())
        }
    }
}