package com.b201crew.protel

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.b201crew.protel.helper.ScreenHelper

class TreatmentSettingsActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var screenHelper:ScreenHelper
    private lateinit var backButton : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_treatment_settings)

        backButton = findViewById(R.id.back_button)
        backButton.setOnClickListener(this)
        screenHelper = ScreenHelper(this)
    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            R.id.back_button -> screenHelper.closeScreen()
        }
    }

}