package com.b201crew.protel

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Build
import android.os.Bundle
import android.os.IBinder
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.b201crew.protel.helper.SharedHelper
import com.b201crew.protel.service.BluetoothService

private const val REQUEST_BT_ENABLED = 6

class MainActivity : AppCompatActivity() {
    private var status = false
    private var mBounded = false
    private var service: BluetoothService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bluetoothCheck()
    }

    //cek kondisi bluetooth
    private fun bluetoothCheck() {
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

        if (bluetoothAdapter?.isEnabled == false) {
            val enableBt = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBt, REQUEST_BT_ENABLED)
        }

        startService()
    }

    private fun startService() {
        val serviceIntent = Intent(this, BluetoothService::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.startForegroundService(serviceIntent)
        } else {
            this.startService(serviceIntent)
        }
    }

    private fun isRunning(): Boolean {
        val sharedHelper = SharedHelper(this)
        return sharedHelper.getServiceStatus()
    }

    private val mConnection = object : ServiceConnection {
        override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
            mBounded = true
            val mLocalBinder = p1 as BluetoothService.LocalBinder
            service = mLocalBinder.getServerInstance()!!
        }

        override fun onServiceDisconnected(p0: ComponentName?) {
            mBounded = false
            service = null
            status = false
        }
    }

    private fun connectService() {
        val serviceIntent = Intent(this, BluetoothService::class.java)
        this.bindService(serviceIntent, mConnection, Context.BIND_AUTO_CREATE)
    }

    //melihat hasil request
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_BT_ENABLED) {
            if (resultCode == Activity.RESULT_OK) {
                startService()
            } else {
                Toast.makeText(this, "this app requires bluetooth to be turned on", Toast.LENGTH_SHORT).show()
                finishAndRemoveTask() //close the app
            }
        }
    }
}
