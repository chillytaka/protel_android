package com.b201crew.protel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentHistoryTimeBinding
import com.b201crew.protel.room.AppDatabase
import java.text.SimpleDateFormat
import java.util.*

class TimeHistoryFragment : Fragment() {

    private lateinit var binding: FragmentHistoryTimeBinding
    private lateinit var appDb: AppDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history_time, container, false)

        appDb = AppDatabase.getInstance(requireContext())

        getTempData()
        return binding.root
    }

    private fun getTempData() {
        val tempDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
        val startDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(tempDate)
        val endDate = Calendar.getInstance()
        endDate.time = startDate!!
        endDate.add(Calendar.DATE, 1)

        setMaxMinText(startDate.time, endDate.timeInMillis)
    }

    fun setMaxMinText(startDate: Long, endDate: Long) {
        val maxTimer = appDb.TimerDao().getMaxTimerFilterByDate(startDate = startDate, endDate = endDate)
        val minTimer = appDb.TimerDao().getMinTimerFilterByDate(startDate = startDate, endDate = endDate)

        binding.longestSitTimeContent.text = getString(R.string.time_left_content, (maxTimer / 60).toString(), (maxTimer % 60).toString())
        binding.shortestSitTimeContent.text = getString(R.string.time_left_content, (minTimer / 60).toString(), (minTimer % 60).toString())
    }
}