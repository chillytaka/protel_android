package com.b201crew.protel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment

class HeaderFragment : Fragment(), View.OnClickListener {
    private lateinit var title:TextView
    private lateinit var logoutButton: ImageButton

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        val contentView = inflater.inflate(R.layout.fragment_header_main, container, false)

        this.title =  contentView.findViewById(R.id.profile_text)
        this.logoutButton = contentView.findViewById(R.id.logout_button)
        logoutButton.setOnClickListener(this)

        return contentView
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.logout_button -> Toast.makeText(context, "logout button clicked", Toast.LENGTH_SHORT).show()
        }
    }
}