package com.b201crew.protel

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.b201crew.protel.databinding.FragmentTreatmentAllBinding
import com.b201crew.protel.helper.SharedHelper

class TreatmentAllFragment : Fragment() {
    private lateinit var binding: FragmentTreatmentAllBinding
    private lateinit var sharedHelper: SharedHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_treatment_all, container, false)

        sharedHelper = SharedHelper(requireContext())
        getData()
        return binding.root
    }

    private fun getData() {
        val treatmentLeft = sharedHelper.getTreatmenPerDay()
        binding.treatmentLeftContent.text = treatmentLeft.toString()
    }
}
